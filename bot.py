#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# bot.py
# This file is part of epol/klarabot
#
# Copyright (C) 2018 - Enrico Polesel
#
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import logging
import threading

import telegram.ext
import sqlalchemy
import sqlalchemy.orm
import sqlalchemy.ext.declarative

logger = logging.getLogger('klarabot')
logger.setLevel(logging.DEBUG)
log_handler = logging.StreamHandler()
log_formatter = logging.Formatter('%(asctime)-19s %(name)-10s %(threadName)-10s %(funcName)-16s %(levelname)-8s %(message)s',datefmt='%Y-%m-%d %H:%M:%S')
log_handler.setFormatter(log_formatter)
logger.addHandler(log_handler)




parser = argparse.ArgumentParser()
parser.add_argument("token",help="Telegram token")
#parser.add_argument("-t",help="Timeout in minutes",required=False,type=int,default=30)

args = parser.parse_args()

logger.debug("Initializing DB")
sql_engine = sqlalchemy.create_engine('sqlite:///db.sqlite')
sql_base = sqlalchemy.ext.declarative.declarative_base()

class User(sql_base):
    __tablename__ = 'users'

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    telegram_id = sqlalchemy.Column(sqlalchemy.String)
    first_name = sqlalchemy.Column(sqlalchemy.String)
    last_name = sqlalchemy.Column(sqlalchemy.String)
    username = sqlalchemy.Column(sqlalchemy.String)
    is_admin = sqlalchemy.Column(sqlalchemy.Boolean)
    is_enabled = sqlalchemy.Column(sqlalchemy.Boolean)

    def __repr__(self):
        return "<User(name: {name},surname: {surname})>".format(name=self.first_name,surname=self.last_name)

    
sql_base.metadata.create_all(sql_engine)
sql_Session = sqlalchemy.orm.sessionmaker(bind=sql_engine)
sql_session = sql_Session()

timers = []

reply_markup=telegram.ReplyKeyboardMarkup( [
    ["Svuotata"],
    ["Accendi"]
])


def start(bot,update):
    tuser = update.message.from_user
    logger.info("New start received from {id} - {name}".format(id=tuser.id,name=tuser.first_name))
    try:
        dbuser = User(telegram_id=tuser.id,first_name=tuser.first_name,last_name=tuser.last_name,username=tuser.username,is_admin=False,is_enabled=True)
        sql_session.add(dbuser)
        sql_session.commit()
    except:
        logger.exception("Error creating a user")
        update.message.reply_text("Error")
    update.message.reply_text("Benvenuto {name}".format(name=tuser.first_name),reply_markup=reply_markup)
                                           
                                    


def avvisa(bot, message):
    global sql_Session
    sql_session = sql_Session()
    logger.debug("Devo avvisare tutti")
    users = sql_session.query(User).filter_by(is_enabled=True).all()
    for user in users:
        logger.debug("Avviso {name}".format(name=user.first_name))
        bot.send_message(chat_id=user.telegram_id,text=message,parse_mode=telegram.ParseMode.MARKDOWN)
        
def start_timer(bot):    
    logger.info("Faccio partire un timer")
    timer = threading.Timer(60.0*30.0,lambda : avvisa(bot,"Qualcuno svuoti klara"))
    timer.start()
    global timers
    timers.append(timer)

def svuotata(bot,update):
    logger.info("La macchina è stata svuotata")
    avvisa(bot,"Klara è stata *svuotata*")
    global timers
    for timer in timers:
        try:
            timer.cancel()
        except:
            pass
    timers = []
    start_timer(bot)


def accendi(bot,update):
    start_timer(bot)
    
def textreply(bot,update):
    text = update.message.text.lower()
    logger.debug("Received text {text}".format(text=str(text)))
    if text == "svuotata":
        svuotata(bot,update)
    elif text == "accendi":
        accendi(bot,update)
    else:
        update.message.reply_text("Che cazzo dici giovine?")
        



logger.info("Creating telegam bot")
updater = telegram.ext.Updater(args.token)
updater.dispatcher.add_handler(telegram.ext.CommandHandler('start', start))
updater.dispatcher.add_handler(telegram.ext.CommandHandler('svuotata', svuotata))
updater.dispatcher.add_handler(telegram.ext.CommandHandler('accendi', accendi))
updater.dispatcher.add_handler(telegram.ext.MessageHandler(telegram.ext.Filters.text, textreply))

updater.start_polling()
logger.info("Polling started")
updater.idle()
